#include <CyberLib.h>
#include <NewPing.h>
#include <Wire.h>
#include <LSM303.h>

LSM303 compass;

#define NORTH 0 // Север.
#define EAST 1  // Восток.
#define SOUTH 2 // Юг.
#define WEST 3  // Запад.

/*     N
 *     |
 * W---|---E
 *     |
 *     S
 */

//++++для сонаров
#define SONAR_NUM 3
#define MAX_DISTANCE 300
#define PING_INTERVAL 50
#define BORDER_MAX_DISTANCE 19
#define BREAK_DISTANCE 19
#define SQUARE_DISTANCE 30  // Размер квадрата (секции) (в см).
#define PULSE_PER_SQUARE 51 // За одну секцию (30 см) происходит 55 смен фронта энкодера, но это число тоже можно менять
#define PULSE_PER_90_DEGREES 11  // Смен фронта за поворот на прямой угол для каждого энкодера (54 - полный круг)
#define TURN_SPEED 50
#define SLOW_TURN_SPEED 45
#define STANDART_SPEED 55
#define SLOW_SPEED 30
#define SIDE_COMPENSATION 15
#define LIMIT_DISTANCE_TIME 3000  // Максимальное время замера (в мкс). 3000 мкс -> 51 см (туда и обратно).
#define QUAN_OF_DIST_MEASURES 9
#define CRITICAL_INCREASE 0.5  // Максимально резкое изменение на ИК-дальномерах, если больше - значит, один из датчиков потерял стенку
#define RIGHT_IR_NORMAL_DISTANCE 49  // Значение на правом ИК при езде по Правомуу ИК
#define LEFT_IR_NORMAL_DISTANCE 49  // Значение на левом ИК при езде по Левому ИК
//----для сонаров

//++++для моторшилда
#define SPEED_1        5
#define DIR_1          4
 
#define SPEED_2        6
#define DIR_2          7

#define FORWARD        0
#define LEFT           2
#define RIGHT          1
#define BACKWARD       3

#define LEFT_TRIG 9
#define LEFT_ECHO 8
#define FORWARD_TRIG 12
#define FORWARD_ECHO 13
#define RIGHT_TRIG 11
#define RIGHT_ECHO 10
#define FRONT_LEFT A2
#define FRONT_RIGHT A3

#define PICKUP 14

//------PID_REGUL MTHRFCK!!!!
// Параметры пропорционального звена
float kp = 0.08; // Коэффициент пропорционального звена
// Параметры интегратора
float ki = 0; // Коэффициент интегрального звена
#define iMin -0.5 // Минимальное значение интегратора 
#define iMax 0.5 // Максимальное значение интегратора
float iSum = 0; // Сумма ошибок (значение, накопленное в интеграторе)
// Параметры дифференциатора
float kd = 0; // Коэффициент дифференциального звена 
float old_y = 0; // Предыдущее значение сигнала
float old_l = 0; // Предыдущее значение сигнала
float old_r = 0; // Предыдущее значение сигнала
float ctrl_speed = 0;

boolean isLeftIRLost, isRightIRLost = false;

float PIDctl(float measureL, float measureR, float y) { 
  float up, ui, ud, error, diffL, diffR;
  diffL = 0.01*(measureL-old_l);
  diffR = 0.01*(measureR-old_r);
  if(diffR > CRITICAL_INCREASE && diffL < CRITICAL_INCREASE && !isRightIRLost) {
    isRightIRLost = true;
    error = LEFT_IR_NORMAL_DISTANCE - measureL;
    kp = 0.1;
    kd = 0;
    ki = 0.04;
  }
  if(diffR < CRITICAL_INCREASE && diffL > CRITICAL_INCREASE && !isLeftIRLost) {
    isLeftIRLost = true;
    error = measureR - RIGHT_IR_NORMAL_DISTANCE;
    kp = 0.1;
    kd = 0;
    ki = 0.04;
  }
  if((isRightIRLost && isLeftIRLost) || (!isRightIRLost && !isLeftIRLost))
  {
    error = measureR - measureL;
    kp = 0.08;
    kd = 0;
    ki = 0;
  }
  else 
    if(isRightIRLost) error = LEFT_IR_NORMAL_DISTANCE - measureL;
    else 
      if(isLeftIRLost) error = measureR - RIGHT_IR_NORMAL_DISTANCE;
//  Serial.println("__________");
//  Serial.print("diffR =");
//  Serial.println(measureR);
//  Serial.print("diffL =");
//  Serial.println(measureL);
//  Serial.println("----------");
//  Serial.print("leftLost =");
//  Serial.println(isLeftIRLost);
//  Serial.print("RightLost =");
//  Serial.println(isRightIRLost);
//  Serial.print("Error =");
//  Serial.println(error);
   
  // Пропорциональная компонента 
  up = kp*error;
  // Интегральная компонента
  iSum = iSum+error; // Накапливаем (суммируем) 
  if(iSum<iMin) iSum = iMin; // Проверяем граничные значение 
  if(iSum>iMax) iSum = iMax;
  ui = ki*iSum;
  // Дифференциальная компонента 
  ud = kd*(y-old_y);
  old_y = y;
  old_r = measureR;
  old_l = measureL;
  return up+ui+ud;
}
//------PID_REGUL MTHRFCK!!!!

boolean motorDirection_1, motorDirection_2;
//----для моторшилда

//++++для сонаров
unsigned long pingTimer[SONAR_NUM];
unsigned int cm[SONAR_NUM];
uint8_t currentSensor = 0;
//NewPing sonar[SONAR_NUM] = 
//  {
//    NewPing(12, 13, MAX_DISTANCE),  // Передний
//    NewPing(11, 10, MAX_DISTANCE),  // Правый
//    NewPing(9, 8, MAX_DISTANCE)     // Левый
//  };

//++++для энкодера
char volatile distanceR = 0; // Количество смен фронта, полученных с правого энкодера
char volatile distanceL = 0; // и с левого
//----для энкодера

//++++для пути
#define CROSS_LEFT 0b10
#define CROSS_FORWARD 0b11
#define CROSS_RIGHT 0b01
uint8_t countOfCross = 0; // Счётчик развилок.
uint8_t arrayOfCross[30]; // Массив для развилок.
/*
 * Структура массива для развилок:
 * Каждый элемент массива представляет собой развилку.
 * Структура каждой развилки: байт (буквы - для нумерации, не hex)
 * 0bABCDEFGH, она разбивается на 2 части: AB и CDEFGH.
 * Первая часть (AB) - текущее выбранное направление движения по этой развилке:
 *   10 - лево,
 *   11 - прямо,
 *   01 - право;
 * Вторая часть разбивается ещё на 3 подчасти (CD, EF и GH), где первая подчасть
 * соответствует направлению налево, вторая - прямо, и третья - право.
 * Первый бит означает, есть ли путь в эту сторону (0, если там стенка, и 1
 * в противном случае), второй - ездили ли мы туда (0 - нет, 1 - да).
 */
uint8_t arrayOfForwardMoves[30]; // Массив для счёта движений прямо после i-ой развилки, использует тот же count, что и для развилок.
uint8_t distance = 0;

uint8_t cmOld = 0;
uint8_t distanceToBorder = 9;
int u = 0;

// для ИК-дальномера
float a = 0.008271;
float b = 939.6;
float c = -3.398;
float d = 17.339;
float arrOfMeasures[QUAN_OF_DIST_MEASURES];
unsigned long startTime, endTime = 0;

int startDegree = 0;
char dir = NORTH;

void dWrite(byte pin, byte val){
  byte bit = digitalPinToBitMask(pin);
  volatile byte *out;
  out = portOutputRegister(digitalPinToPort(pin));
  (val)? *out |= bit : *out &=~bit;
}

unsigned long scanning(uint8_t numOfTrig, uint8_t numOfEcho) {  // Замеры и рассчет дистанции.    !!!Спросить у Дмитрия Витальевича насчет продолжительных active=0 после одного "нулевого" измерения!!!
  int quanOfMeasure = 1;  // Кол-во замеров.
  unsigned long averImpulseTime = 0;  // Переменная для хранения средней длины импульса.
  for(int i=1; i<=quanOfMeasure; i++) {
    digitalWrite(numOfTrig, HIGH);  // Излучаем импульс.
    delayMicroseconds(10);
    digitalWrite(numOfTrig, LOW);  // Прекращаем излучение импульса через 10 мкс.
    unsigned long activeImpulseTime = pulseIn(numOfEcho, HIGH, LIMIT_DISTANCE_TIME);  // Замеряем время прихода i-того импульса.
//    Serial.print("active:");
//    Serial.println(activeImpulseTime); // для отладки
    if (activeImpulseTime == 0)  // Если время, отведенное на замер, превышено,
      averImpulseTime += LIMIT_DISTANCE_TIME / quanOfMeasure;  // используем предельное значение,
    else
      averImpulseTime += activeImpulseTime / quanOfMeasure;  // иначе используем активное, но и там, и там мы сразу считаем среднее значение.
//    delay(50);  // Перерыв между излучениями.
  }
  int distance = averImpulseTime / 58;  // Перевод в сантиметры.
//  Serial.print("distance:");
//  Serial.println(distance);  // для отладки
  return distance;
}

void goForward() {
 /*
  * Перемещение на одну секцию вперёд.
  */
  distanceR += -PULSE_PER_SQUARE;
  distanceL += -PULSE_PER_SQUARE;
  isLeftIRLost = isLeftIRLost = false;
  while(distanceR < 0 && distanceL < 0) {
    int leftIR = IRMesurement('l');
    int rightIR = IRMesurement('r');
    ctrl_speed = PIDctl(leftIR, rightIR, ctrl_speed);
    go(FORWARD, 40+ctrl_speed, 40-ctrl_speed);
  }
//  go(BACKWARD, STANDART_SPEED , STANDART_SPEED);
//  delay(100);

  go(BACKWARD, 0, 0);
  delay(100);
}

void goBackward() {
 /*
  * Перемещение на одну секцию назад.
  */
  go(BACKWARD, 0, 0);
  delay(200);
  distanceR = -distanceR + PULSE_PER_SQUARE;
  distanceL = -distanceL + PULSE_PER_SQUARE;
  
  go(BACKWARD, STANDART_SPEED, STANDART_SPEED);
  while(distanceR > 0 && distanceL > 0);
  go(FORWARD, STANDART_SPEED, STANDART_SPEED);
  delay(100);

  go(BACKWARD, 0, 0);
  delay(50);
}

void stopRobot() {
  go(BACKWARD, 50, 50);
  delay(50);
  go(BACKWARD, 0, 0);
}

void turnLeft() {
  /*
   * Поворот налево на 90 градусов.
   * Сначала поворачиваем налево на нужное количество импульсов,
   * потом гасим скорость путём "газа" в противоположную сторону,
   * и выравнимаем энкодеры до "нулевого" значения.
   */
  char distanceL_old = 0; // Переменные для решения одной проблемы
  char distanceR_old = 0;

  // Поворот на необходимый угол по энкодерам.
  distanceL = PULSE_PER_90_DEGREES;
  distanceR = -(PULSE_PER_90_DEGREES);
  go(LEFT, SLOW_TURN_SPEED, SLOW_TURN_SPEED);
  while(distanceL > 0 && distanceR < 0);
  go(LEFT, 0, 0);
  delay(200);

  if(abs(distanceL) > 1) {
    if(distanceL > 0) {
      go(LEFT, SLOW_TURN_SPEED, 0);
      while(distanceL > 0);
    }
    else {
      go(RIGHT, SLOW_TURN_SPEED, 0);
      while(distanceL < 0);
    }
  }
  delay(50);

  if(abs(distanceR) > 1) {
    if(distanceR > 0) {
      go(RIGHT, 0, SLOW_TURN_SPEED);
      while(distanceR > 0);
    }
    else {
      go(LEFT, 0, SLOW_TURN_SPEED);
      while(distanceR < 0);
    }
  }
  go(FORWARD, 0, 0);
  delay(50);
}

void turnRight() {
  /*
   * Поворот направо на 90 градусов.
   * Сначала поворачиваем налево на нужное количество импульсов,
   * потом гасим скорость путём "газа" в противоположную сторону,
   * и выравнимаем энкодеры до "нулевого" значения.
   */
  char distanceL_old = 0; // Переменные для решения одной проблемы
  char distanceR_old = 0;

  // Поворот на необходимый угол по энкодерам.
  distanceL = -(PULSE_PER_90_DEGREES);
  distanceR = (PULSE_PER_90_DEGREES);
  go(RIGHT, SLOW_TURN_SPEED, SLOW_TURN_SPEED);
  while(distanceL < 0 && distanceR > 0);
  go(RIGHT, 0, 0);
  delay(200);

  if(abs(distanceL) > 1) {
    if(distanceL > 0) {
      go(LEFT, SLOW_TURN_SPEED, 0);
      while(distanceL > 0);
    }
    else {
      go(RIGHT, SLOW_TURN_SPEED, 0);
      while(distanceL < 0);
    }
  }
  delay(50);

  if(abs(distanceR) > 1) {
    if(distanceR > 0) {
      go(RIGHT, 0, SLOW_TURN_SPEED);
      while(distanceR > 0);
    }
    else {
      go(LEFT, 0, SLOW_TURN_SPEED);
      while(distanceR < 0);
    }
  }
  go(FORWARD, 0, 0);
  delay(50);
}

void go(int newDirection, int speedL, int speedR) {
  /*
   * Функция установки необходимых знаений на моторы.
   */

  // Блок выделения логических значений для пинов управления
  // направлением вращения моторов.
  switch (newDirection) {
    case BACKWARD:
      motorDirection_1 = true;
      motorDirection_2 = true;
      break;
    case FORWARD:
      motorDirection_1 = false;
      motorDirection_2 = false;
      break;
    case LEFT:
      motorDirection_1 = false;
      motorDirection_2 = true;
      break;
    case RIGHT:
      motorDirection_1 = true;
      motorDirection_2 = false;
      break;
  }

  // Установка напряжений на пинах, отвечающих за направление.
  if (motorDirection_1) D4_High;
    else D4_Low;
  if (motorDirection_2) D7_High;
    else D7_Low;

  // Установка скорости вращения моторов.
  analogWrite(SPEED_1, speedR);
  analogWrite(SPEED_2, speedL);
}

void encoderL() {
  /*
   * Функция обработки изменения фронта с энкодера левого колеса.
   * Используется при прерывании соответственно с левого энкодера.
   */
  if(motorDirection_2)
    distanceL--;
  else 
    distanceL++;
}

void encoderR() {
  /*
   * То же, что и функция выше, но для правого.
   */
  if(motorDirection_1)
    distanceR--;
  else 
    distanceR++;
}

void algoritm() {
  /*
   * Алгоритм работы разбит на 2 части: от старта до финиша и наоборот.
   * Первая часть (выполняется, пока не найдём красный цвет): 
   * Сначала "смотрим по сторонам", т. е. делаем замеры с датчиков, расположенных
   * слева, справа и тут может быть несколько вариантов развития событий:
   *    Для случая, когда у нас до передней стенки больше одной клетки (нет стенки):
   *    С обеих оставшихся сторон меньше (есть стенки):
   *      Проезжаем вперёд на одну клетку, запоминаем это в массиве прямых движений.
   *    В противном случае:
   *      Это - развилка, записываем в массив развилок текущую (по всем правилам
   *      записи, см. пояснения в объявлении массива), выбираем одно из тех направлений, что
   *      есть и ещё не посещены и проезжаем на клетку в том направлении. 
   *    Для случая, когда спереди стенка есть:
   *    С обеих оставшихся сторон меньше (есть стенки):
   *      Мы в тупике, значит, нужно откатиться до последней развилки, 
   *      в которой есть непосещённые направления и выбираем другое (если же в данной развилке
   *      исследованы все направления, возвращаемся к предшествующей ей развилке и так пока
   *      не найдём, куда ехать).
   *    В противном случае:
   *      Это тоже развилка (даже если только слева или справа есть путь, что не является развилкой,
   *      но так упрощается логика программы), действия - как и в случае развилки выше.
   *      
   * Вторая часть:
   *    Просто едем по собранным данным (кол-во проездов вперёд и направление проезда развилки)
   *    назад.
   */

//  while(!isRedColor()) {
  while(true) {

    // Здесь 3 замера дальномеров.
    cm[LEFT] = scanning(LEFT_TRIG, LEFT_ECHO);
    cm[FORWARD] = scanning(FORWARD_TRIG, FORWARD_ECHO);
    cm[RIGHT] = scanning(RIGHT_TRIG, RIGHT_ECHO);
      
//    Serial.println(cm[LEFT]);
//    Serial.println(cm[FORWARD]);
//    Serial.println(cm[RIGHT]);
    // Случай, когда спереди нет стенки.
    if(cm[FORWARD] >= SQUARE_DISTANCE) {
//      Serial.print("!F");
      if(cm[LEFT] < SQUARE_DISTANCE && cm[RIGHT] < SQUARE_DISTANCE) {
//        Serial.println("LR");
        goForward();
        arrayOfForwardMoves[countOfCross]++;
      }
      else {
//        Serial.println("!(LR)");
        // Создаём временную переменную, чтобы постоянно к массиву не обращаться, вдруг это понижает
        // быстродействие, а потом, по завершении всех вычислений и определений, запишем её содержимое в
        // массив разилок.
        uint8_t tmpCross = 0;
        if(cm[LEFT] >= SQUARE_DISTANCE)
          tmpCross |= 0b00100000;
        if(cm[FORWARD] >= SQUARE_DISTANCE)
          tmpCross |= 0b00001000;
        if(cm[RIGHT] >= SQUARE_DISTANCE)
          tmpCross |= 0b00000010;

        // При возможности ехать прямо - едем прямо, т. к. не тратим времени на поворот, и, если правильно поехали, то в выигрыше.
        if((tmpCross >> 2) & 0b10) {
          tmpCross |= (CROSS_FORWARD << 6);
          tmpCross |= 0b1 << 2;
        }
        // Рассматриваем возможность ехать вправо
        else if ((tmpCross >> 0) & 0b10) {
          tmpCross |= (CROSS_RIGHT << 6);
          tmpCross |= 0b1 << 0;
        }
        // Рассматриваем возможность ехать влево
        else if ((tmpCross >> 4) & 0b10) {
          tmpCross |= (CROSS_LEFT << 6);
          tmpCross |= 0b1 << 4;
        }


        // Поворот в соответствующую сторону (если нужно).
        if((tmpCross >> 6) != CROSS_FORWARD) {
          if((tmpCross >> 6) == CROSS_LEFT) {
            turnLeft();
          }
          else if ((tmpCross >> 6) == CROSS_RIGHT) {
            turnRight();
          }
        }

        // Занесение содержимого временной переменной в массив развилок.
        arrayOfCross[countOfCross] = tmpCross;
        countOfCross++;

        // Проезд одной клетки в соответствующую сторону.
        goForward();
        arrayOfForwardMoves[countOfCross]++;
      }
    }
    // Случай, когда спереди стенка есть.
    else {
//      Serial.print("F");
      
      // Случай тупика.
      if(cm[LEFT] < SQUARE_DISTANCE && cm[RIGHT] < SQUARE_DISTANCE) {
//        Serial.println("LR");
        while(true) {
          
          // Сдаём назад все проеханные вперёд после данной развилки клетки.
          turnLeft();
          turnLeft();
          while(arrayOfForwardMoves[countOfCross]-- > 0) {
            goForward();
            delay(300);
          }

          // Возвратились к предыдущему перекрёстку - декементируем счётчик развязок для доступа к нему.
          countOfCross--;
          
          uint8_t tmpCross = arrayOfCross[countOfCross];
          
          // Разворот в направлении, в котором приехали на этот перекрёсток.
          if((tmpCross >> 6) != CROSS_FORWARD) {
            if((tmpCross >> 6) == CROSS_LEFT)
              turnRight();
            else
              turnLeft();
          }

          // Переменная под то, найдём ли другой путь на этой развилке.
          boolean isHaveAnotherWayHere = false;
          uint8_t way = 0;

          // Ищем ещё один неисследованный путь наэтой развилке.
          for(uint8_t i = 0; i <= 2; i++) {
            if((tmpCross >> (i*2) & 0b11) == 0b10) {
              isHaveAnotherWayHere = true;
              way = i;
              break;
            }
          }

          // Если есть, то поворачиваемся туда и выходим из цикла тупика.
          if(isHaveAnotherWayHere) {
            switch(way) {
              case(0):
                tmpCross |= (CROSS_RIGHT << 6);
                tmpCross |= 0b1 << 0;
                turnRight();
                break;
              case(1):
                tmpCross |= (CROSS_FORWARD << 6);
                tmpCross |= 0b1 << 2;
                break;
              case(2):
                tmpCross |= (CROSS_LEFT << 6);
                tmpCross |= 0b1 << 4;
                turnLeft();
                break;
            }
            arrayOfCross[countOfCross] = tmpCross;
            countOfCross++;
            break;
          }
        }
      }

      // Когда хотя бы с одной из сторон стенки нет - развилка, только без возможности ехать прямо.
      else {
//        Serial.println("!(LR)");
        // Создаём временную переменную, чтобы постоянно к массиву не обращаться, вдруг это понижает
        // быстродействие, а потом, по завершении всех вычислений и определений, запишем её содержимое в
        // массив разилок.
        uint8_t tmpCross = 0;
        if(cm[LEFT] >= SQUARE_DISTANCE)
          tmpCross |= 0b00100000;
        if(cm[FORWARD] >= SQUARE_DISTANCE)
          tmpCross |= 0b00001000;
        if(cm[RIGHT] >= SQUARE_DISTANCE)
          tmpCross |= 0b00000010;

        // При возможности ехать прямо - едем прямо, т. к. не тратим времени на поворот, и, если правильно поехали, то в выигрыше.
        if((tmpCross >> 2) & 0b10) {
          tmpCross |= (CROSS_FORWARD << 6);
          tmpCross |= 0b1 << 2;
        }
        // Рассматриваем возможность ехать вправо
        else if ((tmpCross >> 0) & 0b10) {
          tmpCross |= (CROSS_RIGHT << 6);
          tmpCross |= 0b1 << 0;
        }
        // Рассматриваем возможность ехать влево
        else if ((tmpCross >> 4) & 0b10) {
          tmpCross |= (CROSS_LEFT << 6);
          tmpCross |= 0b1 << 4;
        }


        // Поворот в соответствующую сторону (если нужно).
        if((tmpCross >> 6) != CROSS_FORWARD) {
          if((tmpCross >> 6) == CROSS_LEFT) {
            turnLeft();
          }
          else if ((tmpCross >> 6) == CROSS_RIGHT) {
            turnRight();
          }
        }

        // Занесение содержимого временной переменной в массив развилок.
        arrayOfCross[countOfCross] = tmpCross;
        countOfCross++;

        // Проезд одной клетки в соответствующую сторону.
        goForward();
        arrayOfForwardMoves[countOfCross]++;
      }
    }
  Serial.print("=======");
  Serial.print(countOfCross);
  Serial.println(" block======");
  Serial.print("old_r=");
  Serial.println(old_r);
  Serial.print("old_l=");
  Serial.println(old_l);
  Serial.print("old_y=");
  Serial.println(old_y);
  Serial.println("----------");
  
    Serial.print("cm[LEFT]");
    Serial.println(cm[LEFT]);
    Serial.print("cm[FORWARD]");
    Serial.println(cm[FORWARD]);
    Serial.print("cm[LERIGHTFT]");
    Serial.println(cm[RIGHT]);
  Serial.println("----------");
  Serial.print("leftLost =");
  Serial.println(isLeftIRLost);
  Serial.print("RightLost =");
  Serial.println(isRightIRLost);
  Serial.print("ctrl_speed =");
  Serial.println(ctrl_speed);
  Serial.println("++++++++++++++++++++");
  }
  
  /*
   * Вторая часть.
   * Поскольку и первое действие после обнаружения финиша, и последнее - 
   * это проезд назад накопленых между развилками движений вперёд
   * (т. е. например, если было 3 развилки, то туда - это
   * "вперёд-развилка-вперёд-развилка-вперёд-развилка-вперёд",
   * соответственно и назад всё также, только вперёд <-> назад),
   * что ломает цикл, первый возврат я выполняю отдельно, а потом
   * цикл содержит чередование "развилка-назад" (т. е.
   * для примера выше будет "назад"+3х"развилка-назад".
   */
   
  while(arrayOfForwardMoves[countOfCross]-- > 0) {
    goBackward();
    delay(300);
  }
  
  while(countOfCross > 0) {
    // Возвратились к предыдущему перекрёстку - декрементируем счётчик развязок для доступа к ней.
    countOfCross--;
    
    uint8_t tmpCross = arrayOfCross[countOfCross];
    
    // Разворот в направлении, в котором приехали на этот перекрёсток.
    if((tmpCross >> 6) != CROSS_FORWARD) {
      if((tmpCross >> 6) == CROSS_LEFT)
        turnRight();
      else
        turnLeft();
    }
    
    // Сдаём назад все проеханные вперёд клетки после данной развилки.
    while(arrayOfForwardMoves[countOfCross]-- > 0) {
      goBackward();
      delay(300);
    }
  }
}


void setup() {
  attachInterrupt(1, encoderR, CHANGE);
  attachInterrupt(0, encoderL, CHANGE);
  pinMode(LEFT_TRIG, OUTPUT);
  pinMode(LEFT_ECHO, INPUT);
  pinMode(FORWARD_TRIG, OUTPUT);
  pinMode(FORWARD_ECHO, INPUT);
  pinMode(RIGHT_TRIG, OUTPUT);
  pinMode(RIGHT_ECHO, INPUT);
  pinMode(PICKUP, OUTPUT);
  digitalWrite(PICKUP, HIGH);
  for(int i = 4; i <= 7; i++)
    pinMode(i, OUTPUT);  
  Serial.begin(9600);
  Wire.begin();
  compass.init();
  compass.enableDefault();
  compass.m_min = (LSM303::vector<int16_t>){-2967,  -2519,  -4140}; //откалибровать!
  compass.m_max = (LSM303::vector<int16_t>){+1357,   +265,  -3309}; //откалибровать!
//
  compass.read();
  
  startDegree = compass.heading();
  pinMode(FRONT_LEFT, INPUT);
  pinMode(FRONT_RIGHT, INPUT);
  delay(100);
  go(FORWARD, 0, 0);
}

int IRMesurement (char sensor) {
  float vcc = 0;
  for(int i = 0; i < QUAN_OF_DIST_MEASURES; i++) {
    if(sensor == 'r')
      vcc = 0.004882812 * analogRead(FRONT_RIGHT);
    else if (sensor == 'l')
      vcc = 0.004882812 * analogRead(FRONT_LEFT);
    float distance =  (a + b*vcc)/(1+c*vcc+d*vcc*vcc);
    arrOfMeasures[i] = distance;
  }
    // Сортировака выборки пузырьком
  boolean replaces = true;
  float tmp;
  while(replaces) {
    replaces = false;
    for(int i = 0; i < QUAN_OF_DIST_MEASURES - 1; i++) {
      if(arrOfMeasures[i] > arrOfMeasures[i+1]) {
        tmp = arrOfMeasures[i];
        arrOfMeasures[i] = arrOfMeasures[i+1];
        arrOfMeasures[i+1] = tmp;
        replaces = true;
      }
    }
  }

  float realDistance = 0;
  for(int i = QUAN_OF_DIST_MEASURES / 2 - 1; i <= QUAN_OF_DIST_MEASURES / 2 + 1; i++) {
    realDistance += arrOfMeasures[i+1];
  }
  realDistance /= 3;

  int realDistanceInt = round(realDistance);
  return realDistanceInt;
}

void loop() {
//  startTime = micros();
  int leftIR = IRMesurement('l');
  int rightIR = IRMesurement('r');
//  ctrl_speed = PIDctl(rightIR-leftIR, ctrl_speed);
//  go(FORWARD, 40+ctrl_speed, 40-ctrl_speed);
//  
//  Serial.println(ctrl_speed);
//  endTime = micros() - startTime;
//  
//  Serial.print(leftIR);
//  Serial.print(" - ");
//  Serial.println(rightIR);
//  delay(1);
//
  while(Serial.read() != 'z')
  Serial.print("+");
//  for(int i = 0; i <= 4; i++) 
//    goForward();
//  go(FORWARD, 0, 0);
//  while(true);
  algoritm();

//  Serial.println("++++++++++++++++++++++++++++++++++");
//  Serial.println(PIDctl(leftIR, rightIR, ctrl_speed));
//PIDctl(leftIR, rightIR, ctrl_speed);
}

